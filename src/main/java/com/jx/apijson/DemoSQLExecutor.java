/*Copyright ©2016 TommyLemon(https://github.com/TommyLemon/APIJSON)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

package com.jx.apijson;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import zuo.biao.apijson.server.AbstractSQLExecutor;
import zuo.biao.apijson.server.SQLConfig;

import java.util.List;


/**
 * executor for query(read) or update(write) MySQL database
 *
 * @author Lemon
 */
public class DemoSQLExecutor extends AbstractSQLExecutor {
    private static final Log log = LogFactory.get();
    private static final String TAG = "DemoSQLExecutor";

    @Override
    public List<Record> executeQuery(SQLConfig config) throws Exception {
        String sql = config.getSQL(config.isPrepared());
        System.out.println(sql);
        if (config.getPreparedValueList().size() > 0) {
            return Db.find(sql, config.getPreparedValueList().toArray());
        } else {
            return Db.find(sql);
        }
    }

    @Override
    public int executeUpdate(SQLConfig config) throws Exception {
        return 0;
    }


}
